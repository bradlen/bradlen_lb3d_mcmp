#!/bin/bash

if (( $# < 1 ))
then
    echo "Error: too few parameters!"
    exit
fi

rock_file=`basename $1 .dat`

OUTPUT="output"
INPUT="input-file"
JOBNAME=${rock_file}
JOBDIR=${OUTPUT}/${rock_file}
JOBINPUT=${JOBDIR}/${INPUT}-${JOBNAME}
JOBOUTPUT=${JOBNAME}

mkdir -p ${JOBDIR}

CHECKPOINT=(`ls -rt ${JOBDIR}/checkpoint_${JOBNAME}_*p000000.xdr 2>/dev/null`)
if [ ! -z ${CHECKPOINT} ]
then
	CHECKPOINT=`basename ${CHECKPOINT[@]:(-1)}`
	CHECKPOINT=`echo ${CHECKPOINT} | cut -d'_' -f5`
fi

sed -e "s,^folder.*,folder = '""${JOBDIR}""/'," \
    -e "s/^gr_out_file.*/gr_out_file = '${JOBOUTPUT}'/" \
    -e "s/^obs_file.*/obs_file = '${rock_file}.dat'/" \
    ${INPUT} > ${JOBINPUT}

cp -pf ${HOME}/lb3d_mcmp/src/lbe ${JOBDIR}

echo ${JOBNAME} ${JOBDIR} ${JOBINPUT} ${CHECKPOINT}
