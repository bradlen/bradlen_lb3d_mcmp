#!/bin/bash

LB3D=~/lb3d_mcmp/src/lbe

if [ ! -f ${LB3D} ]; then
  echo "Please place executable at ${LB3D}."
  exit -1
fi

queue=madeinsc
nnodes=4
ncpus=16
mem=4gb
walltime=24:00:00

rock_file=empty.dat
recover=0

SETUP=(`./setup.sh ${rock_file}`)
JOBNAME=${SETUP[0]}
JOBDIR[$index]=${SETUP[1]}
JOBINPUT[$index]=${SETUP[2]}

if ((!recover==0))
then
	CHECKPOINT[$index]=${SETUP[3]}
fi

JOBDIR=$( IFS=: ; echo "${JOBDIR[*]}" )
JOBINPUT=$( IFS=: ; echo "${JOBINPUT[*]}" )
CHECKPOINT=$( IFS=: ; echo "${CHECKPOINT[*]}" )

qsub -N ${JOBNAME:0:15} \
       -q ${queue} \
       -l select=${nnodes}:ncpus=${ncpus}:mpiprocs=${ncpus}:mem=${mem} \
       -l walltime=${walltime} \
       -v JOBDIR=${JOBDIR},JOBINPUT=${JOBINPUT},CHECKPOINT=${CHECKPOINT} \
       run_palmetto.sh
