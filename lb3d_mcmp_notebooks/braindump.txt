Jotting down some bullet points...

* Group meetings
* Journal club
* Computational Science Seminars
* Group web page

* Decide where to host source code
* Decide where to host notebooks
* Digital group library
* Reports, Manuscripts, Paper drafts

* Naming scheme for computers
* Computer lab infrastructure (NFS, NIS, SSH, Backup)

* Project management (Basecamp, Asana, ...?)
